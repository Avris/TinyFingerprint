# 🫵 TinyFingerprint

> a minimalistic, privacy-friendly library for browser fingerprinting in nodejs

modern fingerprinting methods involve complex client-side data-gathering tricks that invade users' privacy.
however, in my experience simply generating a hash based on HTTP headers on the server's side is good enough
for many non-nefarious purposes, like _mitigating_ one person answering the same poll multiple times.

basically, this code looks at a bunch of data that your browser sends to a website when it's requesting a page,
things like the IP address, user-agent and preferred languages.
neither of them is necessarily unique to your device, but if you combine all of them,
that combination becomes _quite_ unique overall.
the library then hashes all that data: it's a process that creates a random-looking string
(like `b5bc6964d26725b5ad085ecf4381f3b4aa797e52c4f8e7402cbc4e4c08ac593d`)
that on one hand stays the same if provided the same data again,
but on another, it's virtually impossible to revert that process and find out eg. someone's IP address from it.

in other words, this library generates a hash allows you to identify,
if a bunch of requests are likely to be coming from the same device and browser,
without having to save any personal data about the user.
and it's all done in a minimalistic fashion – in just a few lines of code and with zero dependencies.

## usage

    const { ip, fingerprint } = require('tinyfingerprint');
    
    export default (req, res, next) => {
        console.log(ip(req));           // eg. 123.45.67.89
        console.log(fingerprint(req));  // eg. b5bc6964d26725b5ad085ecf4381f3b4aa797e52c4f8e7402cbc4e4c08ac593d
        next();
    }

## development

    node tests.js     # run tests
    node homepage.js  # regenerate homepage from readme

## author & links

 - andrea vos
 - [avris.it](https://avris.it)
 - license: [OQL](https://oql.avris.it/license?c=Andrea%20Vos%7Chttps://avris.it)
 - [source code](https://gitlab.com/Avris/TinyFingerprint/-/blob/main/index.js)
 - [npm package](https://www.npmjs.com/package/tinyfingerprint)
