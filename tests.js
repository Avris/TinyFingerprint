const test = require('baretest')('TinyFingerprint'),
    assert = require('assert'),
    TinyFingerprint = require('./index');

const IP_1 = '123.45.67.89';
const IP_2 = '127.0.0.1';
const IP_3 = '13.37.0.1';
const IP_4 = '21.37.69.69';
const IP_5 = '13.12.13.12';

test('ip', async function() {
    assert.equal(
        TinyFingerprint.ip({
            headers: {'x-forwarded-for': IP_1},
            connection: {remoteAddress: IP_2},
            ips: [IP_3, IP_4],
            ip: IP_5,
        }),
        IP_1,
    );
    assert.equal(
        TinyFingerprint.ip({
            headers: {},
            connection: {remoteAddress: IP_2},
            ips: [IP_3, IP_4],
            ip: IP_5,
        }),
        IP_2,
    );
    assert.equal(
        TinyFingerprint.ip({
            headers: {},
            connection: {},
            ips: [IP_3, IP_4],
            ip: IP_5,
        }),
        IP_3,
    );
    assert.equal(
        TinyFingerprint.ip({
            headers: {},
            connection: {},
            ips: [],
            ip: IP_5,
        }),
        IP_5,
    );
    assert.equal(
        TinyFingerprint.ip({
            ip: IP_5,
        }),
        IP_5,
    );
    assert.equal(
        TinyFingerprint.ip({
        }),
        null,
    );
});

test('fingerprint', async function() {
    const req = {
        headers: {
            'x-forwarded-for': IP_1,
            'user-agent': 'foo',
            'accept-language': 'en',
            'accept-encoding': 'gzip,zip',
            'accept': 'html',
        },
        connection: {remoteAddress: IP_2},
        ips: [IP_3, IP_4],
        ip: IP_5,
    };

    const previousFingerprints = [];

    for (let [changeHeader, changeValue] of [
        [null, null],
        ['x-forwarded-for', IP_2],
        ['user-agent', 'bar'],
        ['accept-language', 'en,pl,de,nl,it'],
        ['accept-encoding', 'plaintext,gzip,zip'],
        ['accept', 'html,xml'],
    ]) {
        if (changeHeader) {
            req.headers[changeHeader] = changeValue;
        }
        const fingerprint = TinyFingerprint.fingerprint(req);
        assert.equal(fingerprint.length, 64);
        assert.equal(previousFingerprints.includes(fingerprint), false);
        previousFingerprints.push(fingerprint);
    }
});

test.run();
