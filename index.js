const sha256 = (value) => require('crypto').createHash('sha256').update(value).digest('hex');

module.exports.ip = req => (req?.headers || {})['x-forwarded-for'] || req?.connection?.remoteAddress || (req.ips || [])[0] || req.ip;

module.exports.fingerprint = req => sha256([
    module.exports.ip(req),
    (req?.headers || {})['user-agent'],
    (req?.headers || {})['accept-language'],
    (req?.headers || {})['accept-encoding'],
    (req?.headers || {})['accept'],
].join('|'));
